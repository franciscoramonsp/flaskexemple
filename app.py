from flask import Flask
from helpers.override_functions import render_page_no_cache

app = Flask(__name__)

@app.route('/')
def hello_world():
    return render_page_no_cache('home.html')


if __name__ == '__main__':
    app.run()
