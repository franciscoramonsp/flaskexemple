from flask import render_template
from random import randint

def render_page_no_cache(template,**content):
    return render_template(template,**content,assert_version=randint(1,99999999))

